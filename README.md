# Courses repository


## Get the files

Clone the repository to get the resources

```
cd local_folder
git clone https://gitlab.com/bogdanmarius/courses.git
cd courses
```

## Description
This repository holds scripts and/or programs created/ adapted following online courses

## Usage
The repository is offered as it is.

## Project status
Active :) .
