# import the csv module
import csv

# create new record
newRecord =["1005", "Patrick", "Marleau", "IT"]

# open csv in append mode
file = open("project.csv", "a")  # append to the document

# write to the csv file
wrt = csv.writer(file)

# write the new record in the csv file
wrt.writerow(newRecord)

# close the csv file
file.close()
