# import csv module
import csv

# print(dir(csv))
# print(csv)

# open csv file
file = open("project.csv", "r")

# read the csv file
rdr = csv.reader(file, delimiter=",")

# output csv content
for row in rdr:
    print(row)

# close the csv file
file.close()
