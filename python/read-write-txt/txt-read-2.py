f = open("scratch.txt", "r")

# using a for loop
for x in f:
    print(x.strip())  # using strip to remove empty spaces
