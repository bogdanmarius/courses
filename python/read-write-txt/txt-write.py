# f = open("scratch.txt", "r")  # r = read
# f = open("scratch.txt", "w")  # w = write new line but removes old/all content

f = open("scratch.txt", "a")  # a = append, to add new lines
f.write("\nAugust $12000")  # \n = new line
f.close()
