import mysql.connector

conn = mysql.connector.connect(host='localhost', database='database-name', user='root', password='your-password-here')

if conn.is_connected():
    print("Connected to Mysql DB")

cursor = conn.cursor()

try:
    cursor.execute("insert into db-table-name values (id, 'name', integer-value)")
    conn.commit()
    print("Record added to database")
except:
    conn.rollback()


cursor.close()
conn.close()
