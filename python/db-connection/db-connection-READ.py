import mysql.connector

conn = mysql.connector.connect(host='localhost', database='database-name', user='root', password='your-password-here')

if conn.is_connected():
    print("Connected to Mysql DB")

cursor = conn.cursor()

cursor.execute("select * from db-table-name")

row = cursor.fetchone() # fetch one record at a time
while row is not None:
    print(row)
    row = cursor.fetchone()

rows = cursor.fetcall() # fetch all records at a time
print("Total number of records", cursor.rowcount)
for row in rows:
    print(row)

cursor.close()
conn.close()
