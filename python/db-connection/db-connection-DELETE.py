import mysql.connector

def delete(id):
    conn = mysql.connector.connect(host='localhost', database='database-name', user='root', password='your-password-here')

    if conn.is_connected():
        print("Connected to Mysql DB")
        cursor = conn.cursor()
        str = "delete from db-table-name where id='%d' "
        args = (id)

        try:
            cursor.execute(str % args)
            conn.commit()
            print("Record deleted to database")
        except:
            conn.rollback()
        finally:
            cursor.close()
            conn.close()

inputId = int(input("Enter ID: "))
delete(inputId)
