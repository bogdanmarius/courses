# fetch url and create export file

import urllib.request

try:
    url = urllib.request.urlopen("https://belicos.ro/")
    content = url.read()
    print("Web page found, generating html file")
except urllib.error.URLError:
    print("Web page not found, closing connection")
    exit()

f = open("webpage.html", "wb")
f.write(content)
f.close()
