import smtplib
from email.mime.text import MIMEText
import socket

try:
    body = "This is a test email. How are you?"
    msg = MIMEText(body)
    msg["From"] = "emailaddress@tld.com"
    msg["to"] = "emailaddress@tld.com"
    msg["Subject"] = "Hello message"
    server = smtplib.SMTP("smtp.server.tld", 587)
    server.starttls()
    server.login("emailaddress@tld.com", "thepasswordGoesHere")
    server.send_message(msg)
    print("Email sent")
    server.quit()
except socket.gaierror:
    print("Email not sent")
